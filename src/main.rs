use std::net::UdpSocket;
use std::net::ToSocketAddrs;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    {
        let args: Vec<String> = std::env::args().collect();

        let action = args[1].to_lowercase();
        let location = args[2].to_lowercase();

        if action == "r" || action == "receive" {
            let port: u16 = location.parse()?;
            receive(port)?;
        }
        else if action == "s" || action == "send" {
            send(location, args[3].clone())?;
        }
    }
    Ok(())
}


fn send(target_addr: String, message: String) -> Result<(), Box<dyn std::error::Error>> {
    let socket = UdpSocket::bind("127.0.0.1:34254")?;
    
    let buf = message.as_bytes();

    let addrs_iter = target_addr.to_socket_addrs().unwrap();
    
    let mut target_found = false;
    for target_ip in addrs_iter {
        println!("Trying target_ip: {target_ip}");
        match socket.send_to(buf, target_ip) {
            Ok(_) => {
                println!("Success");
                target_found = true;
                break;},
            Err(error) => {println!("Error: {error}")},
        }
    }
    if !target_found {
        // IPv6 could be used, but the program needs to bind to IPv6
        panic!("Use a hostname or IPv4 address.");
    }

    Ok(())
}

fn receive(port: u16) -> Result<(), Box<dyn std::error::Error>> {
    let addr = "127.0.0.1:".to_owned() + &port.to_string();
    let socket = UdpSocket::bind(addr)?;

    loop {
        // Receives a single datagram message on the socket. If `buf` is too small to hold
        // the message, it will be cut off.
        let mut buf = [0; 10];
        let (amt, _) = socket.recv_from(&mut buf)?;

        // Redeclare `buf` as slice of the received data.
        let buf = &mut buf[..amt];
        println!("{}", String::from_utf8(buf.to_vec())?);
    }
}
