# udp

This is just a testing/learning project for myself not intended for production use. It probably won't be public forever. However if you like it, you're welcome to fork it while it is public.

Is is important to note that UDP packets may get lost or reordered in transmission.

## Examples

### Receive

```bash
cargo run receive 1234
```

OR

```bash
cargo r r 1234
```

### Send

```bash
cargo run send localhost:1234 hi
```

OR

```bash
cargo r s localhost:1234 hi
```

## Notes

Only 10 characters per message are currently supported.
